1) Cookies can be edited by users, because they are stored on the client side, so when you read a variable from a cookie it can be tampered with.

2) Yes, it is still possible because it is still stored in a cookie. However, you need a session ID as well.