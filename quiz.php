<?php
	session_start();
	print_r($_SESSION); echo '<br><br>';
	$name = $_SESSION['name'];
	if (!isset($_SESSION['q_num']))
	{	
		$_SESSION['q_num'] = 1;
	}
	if (!isset($_SESSION['score']))
	{	
		$_SESSION['score'] = 0;
	}
	$q_num = $_SESSION['q_num'];
	$score = $_SESSION['score'];

	include 'question.php';

	require_once('database/db_config.php');
	$db = new PDO($dsn, $user, $pass);

	if (isset($_POST['submit'])) // if we got here by pressing submit
	{
		++$q_num;
		$_SESSION['q_num'] = $q_num;
	}
?>
<html>
<head>
	<title>WebDB</title>
	<link href="stylesheet.css" rel="stylesheet"></link>
</head>
<body>
	<p>
<?php 
	if ($q_num != 1 and isset($_POST['submit']))
	{
		$correct = isCorrect($_POST['answer'], $db);
		$score += $correct;
		echo 'Previous answer was ' . ($correct == 1 ? 'correct' : 'wrong') . '.<br><br>';
		$_SESSION['score'] = $score;
	}
	
	$question = getQuestion($q_num, $db);
	if (!empty($question))
	{
		$choices = getChoices($q_num, $db);

		processQuestion($question['q_text'], $choices, $q_num, $score);
	}
	else
	{
		echo 'Your final score is ' . $score . '.<br><br><a href="dashboard.php">Return</a>.';	
		addAttempt($score, $name, $db);
		unset($_SESSION['q_num']);
		unset($_SESSION['score']);
	}
?>
	</p>
</body>
</html>
