<?php 
	include 'user.php';
	session_start();
	$timeout = 60 * 60 * 24 * 7;

	require_once('database/db_config.php');
	$db = new PDO($dsn, $user, $pass);

	if (isset($_SESSION['timestamp']) and !isset($_POST['submit']))
	{
		$time_elapsed = time() - $_SESSION['timestamp'];
		if ($time_elapsed > $timeout) // session timeout
		{
			session_destroy();
			header('Location: index.php', true);
			die();
		}
		else // continue session
		{
			session_regenerate_id();
			$name = $_SESSION['name'];
			$back = true;
			$success = true;
		}
	}
	elseif (isset($_POST['submit']))
	{
		$name = $_POST['name'];
		if ($_POST['submit'] == 'register')
		{
			session_regenerate_id();
			$success = register($_POST['name'], $_POST['password'], $db);
			$_SESSION['name'] = $name;
		}
		elseif ($_POST['submit'] == 'login')
		{
			session_regenerate_id();
			$success = login($_POST['name'], $_POST['password'], $db);
			$_SESSION['name'] = $name;
		}
		else
		{
			$success = false;
		}
	}
	else
	{
		$success = false;
	}

	if (!$success)
	{
		session_destroy();
		header('Location: index.php', true);
		die();
	}

	$_SESSION['timestamp'] = time();
?>

<html>
<head>
	<title>WebDB</title>
	<link href="stylesheet.css" rel="stylesheet"></link>
</head>
<body>
	Welcome <?=($back ? 'back ' : '')?><?=$name?>.<br><br>
	<a href="quiz.php">Start quiz</a>.<br><br>
	<a href="index.php?logout=1">Logout</a>.<br><br>
	List of attempts:
	<?php listAttempts($name, $db); ?><br>
	Hall of fame:
	<?php listHallOfFame($db); ?><br>
</body>
</html>