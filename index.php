<?php 
	session_start();

	if(isset($_GET['logout']))
	{
		session_destroy();
	}
	
	if(isset($_SESSION['name']))
	{
		header('Location: dashboard.php', true);
		die();
	}

	session_destroy();
	session_start();
?>

<html>
<head>
	<title>WebDB</title>
	<link href="stylesheet.css" rel="stylesheet"></link>
</head>
<body>
	<p>
<?php
	if (!isset($_GET['page'])): 
?>
		<a href="index.php?page=register">register</a><br><br>
		<a href="index.php?page=login">login</a><br>
<?php 
	else: 
?>
		<form action="dashboard.php" method="post">
			Username: <input type="text" name="name"> <br>
			Password: <input type="password" name="password"> <br>
<?php 
		if ($_GET['page'] == 'register'):
?>
			<input type="submit" name="submit" value="register">
<?php 
		elseif ($_GET['page'] == 'login'):
?>
			<input type="submit" name="submit" value="login">
		</form>
<?php
		endif;
	endif;
?>	
	</p>
</body>
</html>