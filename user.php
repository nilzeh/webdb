<?php 
	function register($name, $password, $db)
	{
		// hash the password with a salt
		$salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));
		$password = hash('sha512', $password . $salt);

		$query = $db->prepare('INSERT INTO user (name, password, salt) VALUES (?,?,?)');
		$res = $query->execute(array($name, $password, $salt));
		return $res;
	}

	function login($name, $password, $db)
	{
		$query = $db->prepare('SELECT password, salt FROM user WHERE name = ?');
		$query->execute(array($name));
		$credentials = $query->fetch();
		// generate hashed password with salt
		$password = hash('sha512', $password . $credentials['salt']);
		return $credentials['password'] == $password;
	}

	function listAttempts($name, $db)
	{
		$query = $db->prepare('SELECT * FROM attempt WHERE made_by = ? ORDER BY score DESC');
		$query->execute(array($name));
		$attempts = $query->fetchAll();
		echo '
		<table>
			<tr>
				<td>
					Date
				</td>
				<td>
					Score
				</td>
			</tr>
		';
		foreach ($attempts as $attempt) {
			echo '
			<tr>
				<td>
			' .
			$attempt['made_on']
			. '
				</td>
				<td>
			' .
			$attempt['score']
			. '
				</td>
			</tr>
			';
		}
		echo '
		</table>
		';
	}

	function listHallOfFame($db)
	{
		$query = $db->prepare('SELECT score, made_on, made_by FROM attempt ORDER BY score DESC LIMIT 5');
		$query->execute();
		$scores = $query->fetchAll();
		echo '
		<table>
			<tr>
				<td>
					Date
				</td>
				<td>
					Score
				</td>
				<td>
					Made by
				</td>
			</tr>
		';
		foreach ($scores as $score) {
			echo '
			<tr>
				<td>
			' .
			$score['made_on']
			. '
				</td>
				<td>
			' .
			$score['score']
			. '
				</td>
				<td>
			' .
			$score['made_by']
			. '
				</td>
			</tr>
			';
		}
		echo '
		</table>
		';
	}
?>