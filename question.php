<?php
function isCorrect($c_num, $db)
{
	$query = $db->prepare('SELECT correct FROM choice WHERE c_number = ?');
	$query->execute(array($c_num));
	return $query->fetch()['correct'];
}

function getQuestion($q_num, $db)
{
	$query = $db->prepare('SELECT q_text FROM question WHERE q_number = ?');
	$query->execute(array($q_num));
	return $query->fetch();
}

function getChoices($q_num, $db)
{
	$query = $db->prepare('SELECT c_text,c_number FROM choice WHERE q_number = ?');
	$query->execute(array($q_num));
	return $query->fetchAll();
}

function processQuestion($question, $choices, $q_num, $score)
{
	echo $question . 
	'<form action="quiz.php" method="post">';
	foreach ($choices as $choice)
	{
		echo '<input type="radio" name="answer" value="' . 
			$choice['c_number'] . '"/>' .  $choice['c_text'] . '<br>' ;
	}
	echo '
		<br>
		<input type="submit" name="submit" value="check">
		</form>';
}

function addAttempt($score, $made_by, $db)
{
	$query = $db->prepare('INSERT INTO attempt (score, made_by) VALUES (?,?)');
	$query->execute(array($score, $made_by));
}
?>