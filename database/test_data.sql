USE Questions;

DELETE FROM attempt;
DELETE FROM user;
DELETE FROM choice;
DELETE FROM question;

INSERT INTO question
	(q_text)
	VALUES
	("2 + 2 = ...");

SET @question := LAST_INSERT_ID();

INSERT INTO choice
	(q_number, c_text, correct)
	VALUES
	(@question, "3", false);

INSERT INTO choice
	(q_number, c_text, correct)
	VALUES
	(@question, "4", true);

INSERT INTO choice
	(q_number, c_text, correct)
	VALUES
	(@question, "5", false);

INSERT INTO question
	(q_text)
	VALUES
	("4 + 8 = ...");

SET @question := LAST_INSERT_ID();

INSERT INTO choice
	(q_number, c_text, correct)
	VALUES
	(@question, "6", false);

INSERT INTO choice
	(q_number, c_text, correct)
	VALUES
	(@question, "10", false);

INSERT INTO choice
	(q_number, c_text, correct)
	VALUES
	(@question, "11", false);

INSERT INTO choice
	(q_number, c_text, correct)
	VALUES
	(@question, "12", true);

INSERT INTO choice
	(q_number, c_text, correct)
	VALUES
	(@question, "13", false);

INSERT INTO question
	(q_text)
	VALUES
	("9 - 4 = ...");

SET @question := LAST_INSERT_ID();

INSERT INTO choice
	(q_number, c_text, correct)
	VALUES
	(@question, "5", true);

INSERT INTO choice
	(q_number, c_text, correct)
	VALUES
	(@question, "6", false);

INSERT INTO choice
	(q_number, c_text, correct)
	VALUES
	(@question, "7", false);

INSERT INTO choice
	(q_number, c_text, correct)
	VALUES
	(@question, "8", false);

INSERT INTO user
	(name, password, salt)
	VALUES
	("Hans", "c150abfd2fd8ca5fa4bd3eadf94f02c7b574defdd6c8cb9940fd1bbd322d158ac8981f28bc0190c8037bc555dfe944e28a2a0d39e7cbcc743638b089f7bc5a99", "c09468df54ccb65b939238cc9136d6139a585ad49de661a91b9cb14351f992057caabb1c007ed6a4bb78caf19a11974e25c4dcd0053760b9b3df98ff8d72443e");

INSERT INTO user
	(name, password, salt)
	VALUES
	("Sjon", "b7f8374a04cdb3b7c36346bbb3c717fde45de837010203d4aa2c70d079814380dbb3bcae9eacfdbef40f43f849536ed9b7d6e941631725e46ee030671f1075f1", "b3b4812efcfa636ee0e21cd471e556ff725ff03fa1932a1f3df391300f66941d3eff0dca29e3da0a7588daca33204b7057d156e1e1ca5f28cd973679e2a6204f");

INSERT INTO user
	(name, password, salt)
	VALUES
	("Sjans", "91d6f1d7265f9a67370a3ef126e75a9fd3c8150ba444e899b46b3bffd77def6b321b16b5edc60af86c34635553662d4e4c0cc8cbcdd7df3189980eff51f41ceb", "83e2c3da5dd21fb531a2c052dcefbcd6ed61a04f5101ba7afbf723fe940801b4ca27702c76051bf10b8d7f5b8c2bb4096a539ce4cad25e8d6737b9eeae9f45cd");

INSERT INTO attempt
	(score, made_by, made_on)
	VALUES
	(3, "Hans", CURRENT_TIMESTAMP + INTERVAL 12 HOUR);

INSERT INTO attempt
	(score, made_by, made_on)
	VALUES
	(1, "Hans", CURRENT_TIMESTAMP + INTERVAL 10 HOUR);

INSERT INTO attempt
	(score, made_by, made_on)
	VALUES
	(2, "Sjon", CURRENT_TIMESTAMP + INTERVAL 15 HOUR);

INSERT INTO attempt
	(score, made_by, made_on)
	VALUES
	(1, "Sjon", CURRENT_TIMESTAMP + INTERVAL 120 HOUR);

INSERT INTO attempt
	(score, made_by, made_on)
	VALUES
	(3, "Sjans", CURRENT_TIMESTAMP + INTERVAL 12 YEAR);

