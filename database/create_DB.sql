DROP DATABASE IF EXISTS Questions;

CREATE DATABASE Questions;
USE Questions;
-- USE s2192659;

CREATE TABLE IF NOT EXISTS question
(
	q_number INT NOT NULL AUTO_INCREMENT,
	q_text TEXT NOT NULL,
	PRIMARY KEY (q_number)
);

CREATE TABLE IF NOT EXISTS choice
(
	c_number INT NOT NULL AUTO_INCREMENT,
	q_number INT NOT NULL,
	c_text TEXT NOT NULL,
	correct BOOL NOT NULL,
	PRIMARY KEY (c_number, q_number),
	FOREIGN KEY (q_number)
		REFERENCES question(q_number) -- "part of"
);

CREATE TABLE IF NOT EXISTS user
(
	name CHAR(255) NOT NULL,
	password CHAR(255) NOT NULL,
	salt CHAR(255) NOT NULL,
	PRIMARY KEY (name)
);

CREATE TABLE IF NOT EXISTS attempt
(
	score INT NOT NULL,
	made_by CHAR(255) NOT NULL,
	made_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (made_by, made_on),
	FOREIGN KEY (made_by)
		REFERENCES user(name)
);